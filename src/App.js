import logo from "./logo.svg";
import "./App.css";
import Border from "./components/border";
import { Primeiro } from "./components/primeiro";
import { Segundo } from "./components/segundo";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>
          <Border className="Border" title="Primeiro teste">
            <Primeiro cor="blue"></Primeiro>
          </Border>
          <Border className="Border" title="Segundo teste">
            <Segundo fontWeight="bold">Segundo componente</Segundo>
          </Border>
        </h1>
      </header>
    </div>
  );
}

export default App;
