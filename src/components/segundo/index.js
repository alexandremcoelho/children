import { Component } from "react";

export class Segundo extends Component {
  render() {
    return (
      <div style={{ fontWeight: `${this.props.fontWeight}` }}>
        {this.props.children}
        {console.log(this.props.children)}
      </div>
    );
  }
}
