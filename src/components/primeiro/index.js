import { Component } from "react";

export class Primeiro extends Component {
  render() {
    return (
      <div style={{ color: `${this.props.cor}` }}>Primeiro componente</div>
    );
  }
}
